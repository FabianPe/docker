#!/bin/sh
cp /codeclimate.yml /code/.codeclimate.yml
cp /myRuleset.xml /code/myRuleset.xml
cp -R /.phan /code/

/usr/src/app/bin/codeclimate version
/usr/src/app/bin/codeclimate analyze -f json > codeclimate-report.json

jq -c 'map(select(.type | test("issue"; "i")))' codeclimate-report.json > gl-code-quality-report.json
