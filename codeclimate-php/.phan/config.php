<?php
return [
    'target_php_version' => '7.2',
    "allow_missing_properties" => false,
    "null_casts_as_any_type" => false,
    'backward_compatibility_checks' => false,
    "quick_mode" => false,
    "minimum_severity" => 7,
    'parent_constructor_required' => [
    ],
    'directory_list' => [
        '/code/backend',
        '/code/vendor',
        '.phan/stubs',
        '/code/backend/vendor'
    ],
    "exclude_analysis_directory_list" => [
        '/code/vendor',
        '/code/.phan/stubs',
        '/code/backend/vendor'
    ]
];
